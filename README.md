# 3d动画生成2d序列帧工具

#### 介绍
三维动画加后处理生成二维序列帧

#### 功能说明
将三维动画转为2d帧动画，可以自行定制后处理流程，内置像素风格转换器。

![原图](https://images.gitee.com/uploads/images/2021/0408/223605_123fb07a_5469408.png "场景.png")

![预览界面](https://images.gitee.com/uploads/images/2021/0408/223636_4d1db047_5469408.png "game视图预览.png")

![效果](https://images.gitee.com/uploads/images/2021/0409/001845_f7feab2b_5469408.gif "最终效果.gif")
#### 使用说明

1.  创建配置文件

![创建配置文件](https://images.gitee.com/uploads/images/2021/0409/203946_3581a218_5469408.png "创建配置文件.png")

2.  添加摄像机脚本

![添加摄像机脚本](https://images.gitee.com/uploads/images/2021/0409/204208_27becc14_5469408.png "添加摄像机脚本.png")

3.  点击运行即可录制


