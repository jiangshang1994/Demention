﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Demention
{
    [CreateAssetMenu(fileName ="defaultRecordName",menuName = "录制/创建配置")]
    public class RecordSettings : ScriptableObject
    {
        public AnimationClip[] _animationClips;
        public int _frameRate;
        public string _path;
        public Material[] _material;
    }
}
