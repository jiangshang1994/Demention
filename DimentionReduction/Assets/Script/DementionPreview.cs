using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Demention
{
    [DisallowMultipleComponent]
    [ExecuteInEditMode]
    public class DementionPreview : MonoBehaviour
    {
        public RecordSettings RecordSettings;
        void Start()
        {

        }

        private void Update()
        {
            DementionCameral dementionCameral = GetComponent<DementionCameral>();
            if (dementionCameral == null)
            {
                dementionCameral = gameObject.AddComponent<DementionCameral>();
            }
            RecordSettings = dementionCameral.RecordSettings;
            dementionCameral.SetMatial(RecordSettings);
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            RenderTexture texture = source;
            RenderTexture renderTexture;
            RenderTexture renderTextureTemp = new RenderTexture(texture.width, texture.height, 0);
            Graphics.Blit(texture, renderTextureTemp, RecordSettings._material[0]);
            for (int i = 1; i < RecordSettings._material.Length; i++)
            {
                renderTexture = renderTextureTemp;
                renderTextureTemp = new RenderTexture(texture.width, texture.height, 0);
                Graphics.Blit(renderTexture, renderTextureTemp, RecordSettings._material[i]);
                DestroyImmediate(renderTexture);
            }
            Graphics.Blit(renderTextureTemp, destination);
            DestroyImmediate(renderTextureTemp);
        }
    }
}
