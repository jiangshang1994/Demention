Shader "Hidden/RecodeShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;

            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 scrPos : TEXCOORD1;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.scrPos = ComputeScreenPos(o.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _CameraDepthTexture;
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                float depthValue = Linear01Depth(tex2D(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)).r);
                float alpha;
                if (depthValue == 1)
                {
                    alpha = 0;
                    col = fixed4(0,0,0, alpha);
                }
                else 
                {
                    alpha = 1;
                    col = fixed4(col.rgb, alpha);
                }
                return col; 

            }
            ENDCG
        }
    }
}
