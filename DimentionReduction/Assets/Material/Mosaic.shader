Shader "Hidden/Mosaic"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}

        _RowMosaic("ColmunCount",Range(0,500)) = 250
        _CloumnMosaic("RowCount",Range(0,500)) = 250
        _Color("Color",Color) = (1,1,1, 1)

    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            int _RowMosaic;
            int _CloumnMosaic;
            int _MainTxtWidth;
            int _MainTxtHeight;
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;

            float4 _Color;
            float4 frag (v2f i) : SV_Target
            {
                int _u = trunc(i.uv.x * _RowMosaic);
                int _v = trunc(i.uv.y * _CloumnMosaic);
                fixed4 col = tex2D(_MainTex, float2(_u  /(float)_RowMosaic ,_v/ (float)_CloumnMosaic));
                return float4(col.r * _Color.r, col.g * _Color.g, col.b * _Color.b, col.a);

            }
            ENDCG
        }
    }
}
